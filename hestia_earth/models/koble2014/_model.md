# Koble (2014)

This model estimates the amount of crop residue burnt and removed using country average factors for different crop groupings. The data are described in [The Global Nitrous Oxide Calculator – GNOC – Online Tool Manual](https://gnoc.jrc.ec.europa.eu/documentation/The_Global_Nitrous_Oxide_Calculator_User_Manual_version_1_2_4.pdf).
