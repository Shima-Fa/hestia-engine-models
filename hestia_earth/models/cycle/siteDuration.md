## Site duration

This model calculates the `siteDuration` on the `Cycle` to the same value as `cycleDuration`
when only a single `Site` is present.

### Returns

- a `number` or `None` if the `Cycle` has multiple `Sites`

### Requirements

* A [Cycle](https://hestia.earth/schema/Cycle) with:
  - [cycleDuration](https://hestia.earth/schema/Cycle#cycleDuration) with `365`
  - none of:
    - [otherSites](https://hestia.earth/schema/Cycle#otherSites)

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.cycle import run

print(run('siteDuration', Cycle))
```
