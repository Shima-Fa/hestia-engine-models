# LC-Impact (all effects, 100 years)

These models characterise emissions and resource use according to the methods defined by the [LC-Impact](https://LC-Impact.eu) working group. All the effects caused by an impact category that are known to damage one or more areas of protection are considered, and the time horizon is 100 years.
